//
//  LoginViewController.m
//  AccountManagement
//
//  Created by WeiHu on 15/6/25.
//  Copyright (c) 2015年 WeiHu. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *registerBtn;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.registerBtn.layer.borderColor = [UIColor redColor].CGColor;
}
- (IBAction)loginBtnAction:(UIButton *)sender {
    
    //账号 :huwei    密码 :H12345_90
    [SVProgressHUD show];
    [AVUser logInWithUsernameInBackground:self.userNameTextField.text password:self.pwdTextField.text block:^(AVUser *user, NSError *error) {
        if (user != nil) {
            //登陆成功
            [UIApplication sharedApplication].keyWindow.rootViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RootTabBarViewController"];
            [SVProgressHUD showSuccessWithStatus:@"登录成功"];
        } else {
            //登陆失败
            [SVProgressHUD showErrorWithStatus:@"登录失败"];
        }
    }];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
